function fig = plotSigStars(fig,stats)
%PLOTSIGSTARS adds significance stars to an existing HISTBOX plot.
%   PLOTSIGSTARS(fig,stats) plots significance stars onto the figure handle
%   contained in fig. 
%   Stats is a cell vector containing pairwise grouping tags and p value
%   vectors. Typical use is to match the grouping tags to those obtained or
%   determined in histbox in order to plot stars above the relevent
%   boxplot. In the case of two groups with pairwise comparisons, label a
%   first group 'pairwise'. 
%   stats assumes that the rightmost-plot contains a control group to
%   compare against.
%   for each group, a vector or matrix may be used. NxM matrices will be
%   interpreted as n p values by m statistical tests (for example, to test
%   normality, equality of variance, and t-test), and further tests will be
%   displayed likewise vertically downwards of previous tests.

% Config:
varargs = {'FontName','Helvetica'};%,...
%    'FontSize',12};
xStarDiffPos = 1.5;
yStarPos = 0.97;
xStarContraPos = 0.8;
xStarIpsiPos = 1.8;

nPlots = length(fig.Children);
plotLims = fig.Children(9).YLim;
minHeight = plotLims(1);
plotRange = diff(plotLims);

if contains(stats{1},'pairwise')
    pw = stats{2};
    stats(1:2) = [];
    for ii = 1:length(pw)
        plotPos = nPlots-3-6*(ii-1);
        [t, tOffset,sz] =  starString(pw(ii));
        if strcmp(fig.Children(ii).YScale,'log')
            yPlot = 10^((yStarPos+tOffset)*log10(plotRange+minHeight));
        else
            yPlot = (yStarPos+tOffset)*plotRange+minHeight;
        end
        text(fig.Children(plotPos),xStarDiffPos,yPlot,t,varargs{:},...
            'HorizontalAlignment','center','FontSize',sz);
        if strcmp(fig.Children(ii).YScale,'log')
            yPlot = 10^((yStarPos)*log10(plotRange+minHeight));
        else
            yPlot = (yStarPos-0.01)*plotRange+minHeight;
        end
        l=line(fig.Children(plotPos),[0.75 2.25],[yPlot yPlot]);
        l.Color = 'black';
    end
end
% Star sign loops first
%Hemisphere differences
% sigPos = 1;
% for ii = 3:6:nPlots
%     [t, tOffset,sz,xOff] =  starString(sig(sigPos));
%     if strcmp(fig.Children(ii).YScale,'log')
%         yPlot = 10^((yStarPos+tOffset)*log10(plotRange+minHeight));
%     else
%         yPlot = (yStarPos+tOffset)*plotRange+minHeight;
%     end
%     text(fig.Children(ii),xStarDiffPos+xOff,yPlot,t,varargs{:},'FontSize',sz);
%     sigPos = sigPos+1;
%     if strcmp(fig.Children(ii).YScale,'log')
%         yPlot = 10^((yStarPos)*log10(plotRange+minHeight));
%     else
%         yPlot = (yStarPos-0.01)*plotRange+minHeight;
%     end
%     l=line(fig.Children(ii),[xStarContraPos xStarIpsiPos+.1]+0.1,[yPlot yPlot]);
%     l.Color = 'black';
% end

%control differences
for kk = 1:size(stats{2},2)
yStarPos = 0.97-0.035*kk;
for jj = 1:2:length(stats)
xStarInd = find(contains(fig.Children(3).XTickLabel,stats{jj}));
for ii = 1:length(stats{2})%9:6:nPlots
    plotPos = nPlots-3-6*(ii-1);
    [t, tOffset,sz] =  starString(stats{jj+1}(ii,kk));
    if strcmp(fig.Children(ii).YScale,'log')
        yPlot = 10^((yStarPos+tOffset-.02)*log10(plotRange+minHeight));
    else
        yPlot = (yStarPos+tOffset)*plotRange+minHeight;
    end
    text(fig.Children(plotPos),xStarInd,yPlot,t,varargs{:},...
        'HorizontalAlignment','center','FontSize',sz);
end
end
end

end

function [t, tOffset,sz] = starString(thisSig)
    if thisSig > 0.05
        t = 'NS';
        tOffset = 0.01;
        sz = 5;
    elseif thisSig <= 0.05 && thisSig > 0.01
        t = '*';
        tOffset = 0;
        sz = 7;
    elseif thisSig <= 0.01 && thisSig > 0.001
        t = '**';
        tOffset = 0;
        sz = 6;
    elseif thisSig <= 0.001
        t = '***';
        tOffset = 0;
        sz = 5;
    else
        error('Did you try using a number for your significance value?')
    end
end
